const jwt=require('jsonwebtoken');
const errorMessages=require('../error-codes');
const UserModel=require('../models/user');
const bcrypt=require('bcryptjs');

module.exports=(req,res,next)=>{
   
    const token=req.headers['x-access-token']||req.body['token'];
        if(token!=null){
            jwt.verify(token,req.app.get('apiSecretKey'),(err,resultDecode)=>{
                if(err){
                    res.status(401).send('TOKEN NOT AUTHENTICATED');
                }
                else{
                    const promise=UserModel.findById(resultDecode.id);
                    promise.then((user)=>{
                        if(!user){
                           res.status(401).send('NO USER WITH REQUEST USER ID');
                        }
                            res.locals.user=user;
                                next();
                  
                    }).catch((err)=>{
                    console.log(err)

                        res.status(401).send('NO USER WITH REQUEST USER ID');
                    })
            
                }
            });
        }
        else{
                res.status(401).send('NO TOKEN ERROR 2');
        }

    }



