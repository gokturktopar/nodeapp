const path = require('path');

let rootFolder=path.dirname(require.main.filename);
let rootFolderArr=rootFolder.split('/');
rootFolderArr.pop();//bin
rootFolderArr.pop();//back-end
let uploadFilePath=rootFolderArr.join('/');
uploadFilePath=uploadFilePath+'/uploadedFiles';

module.exports=uploadFilePath;