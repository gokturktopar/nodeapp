const mongoose=require('mongoose');

module.exports=()=>{
/*     
    const uri = 'mongodb://localhost:27017/sidingturk';
    const opts = {
        useNewUrlParser: true, 
        useUnifiedTopology: true,
        useFindAndModify:false,
        useCreateIndex:true,
        replicaSet: 'rs0', 
        readConcern: { 
          level: 'majority'
        }
      };
      mongoose.connect(uri,opts).catch((err)=>{
          console.log(err);
      }) */
          
    const uri = 'mongodb://localhost:27017,localhost:27018,localhost:27019/sidingturk';
    // const uri = 'mongodb://vertiadmin:vertiservice2020;@40.113.152.227:27017/verti';
     const opts = {
         useNewUrlParser: true, 
         useUnifiedTopology: true,
         useFindAndModify:false,
         useCreateIndex:true,
         replicaSet: 'rs0', 
         readConcern: { 
           level: 'majority'
         }
       };
       mongoose.connect(uri,opts).catch((err)=>{
           console.log(err);
       })
 
 
    mongoose.connection.once('open', ()=> {
        console.log('MongoDB event open');
        console.log('MongoDB connected [%s]');

        mongoose.connection.on('connected', function() {
            console.log('MongoDB event connected');
        });

        mongoose.connection.on('disconnected', function() {
            console.log('MongoDB event disconnected');
        });

        mongoose.connection.on('reconnected', function() {
            console.log('MongoDB event reconnected');
        });

        mongoose.connection.on('error', function(err) {
            console.log('MongoDB event error:zczxvxzvzxvzxvvvvvv ');
        });
    });


}