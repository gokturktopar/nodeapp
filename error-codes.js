/**
 * Keeps the custom error codes.
 */


module.exports.ERROR=(m)=>{ 
   return 'Something Went Wrong!'
}

module.exports.NO_TOKEN_AUTHANTICATION_ERROR=()=>{
  return  'There is no token in request.'
    
}
module.exports.TOKEN_AUTHANTICATION_ERROR=()=>{
   return  'User is not be authenticated.'
    
}
module.exports.NO_RESPONDING_DATA_ERROR=()=>{ 
 return 'Update/Delete fail.There is no data responding to request.'
    
}
