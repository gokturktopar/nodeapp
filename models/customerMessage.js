const moongose=require('mongoose');
const Schema=moongose.Schema;

const customerMessageSchema=new Schema({
    nameSurname:String,
    date:String,
    phoneNumber:String,
    email:String,
    comment:String 
});


module.exports=moongose.model('CustomerMessage',customerMessageSchema);





    
