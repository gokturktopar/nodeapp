const moongose=require('mongoose');
const Schema=moongose.Schema;

const userSchema=new Schema({
    username:{
        type:String,
        required:[true,'Username field can not be empty'],
    },
    password:{
        type:String,
        required:[true,'Password field can not be empty'],
    },
});

module.exports=moongose.model('User',userSchema);





    
