var express = require('express');
var router = express.Router();
const siteVariableModel=require('../models/siteVariable')
const customerMessageModel=require('../models/customerMessage')
const customerCommentModel=require('../models/customerComment')
const uploadedFilePath=require('../helper/uploadfilepath');
const fs=require('fs');
const documentModel=require('../models/document');

/* GET NUMBERS */
router.get('/numbers', function(req, res, next) {
    const getVariable=siteVariableModel.findOne({}, {}, { sort: { 'created_at' : -1 } })
    getVariable.then((variable)=>{
      res.json(variable)
    }).catch((err=>{
      res.status(500).send(errorMessages.ERROR());
    }))
  });

/* POSTS NEW MESSAGES */
router.post('/message', function(req, res, next) {
  const newMessage=new customerMessageModel(req.body);
  const promise=newMessage.save();
  promise.then((data)=>{
      res.json('ADDED');
     
  }).catch((err)=>{
    res.status(500).send(errorMessages.ERROR());
  });
});
/* GET ALL COMMENTS */
router.get('/comments', function(req, res, next) {
  customerCommentModel.find({}).then((data)=>{res.json(data)}).catch(err=>res.status(505).errorMessages.ERROR())
});

/*
  Get COMMENT PHOTOS
*/
router.get('/comments/photo/:commentId',(req,res,next)=>{
  const commentId=req.params.commentId;
  const directoryPath=uploadedFilePath+'/comments/'+commentId+'/';
  if (!fs.existsSync(directoryPath)){
      res.json('No Document');
  }
  else{
      fs.readdir(directoryPath, function (err, files) {  
          if (err) {
          res.json('No Document');
              return console.log('Unable to scan directory: ' + err);
          } 
          let documents=[];
          //listing all files using forEach
          files.forEach(function (file) {
              const doc=new documentModel();
              doc.name=file;
              doc.path=directoryPath+'/'+file;
              documents.push(doc);
          });
          res.json(documents);
      });
  }

 
})


  /**DOWNLOAD  COMMENT PHOTO */
router.get('/comments/photo/download/:commentId/:photoName', function(req, res){
    try {
      const commentId=req.params.commentId;
      const photoName=req.params.photoName;
      let file = uploadedFilePath+'/comments/'+commentId+'/'+photoName
      res.download(file);  
    } catch (error) {
    }
});
/**GALLERY */
//get all photos
router.get('/gallery',(req,res,next)=>{
  const directoryPath=uploadedFilePath+'/gallery/';
  if (!fs.existsSync(directoryPath)){
    res.json(-1);
      //res.status(500).send('No Document For Customer');
  }
  else{
      fs.readdir(directoryPath, function (err, files) {  
          if (err) {
            res.json(-1);
              
              return console.log('Unable to scan directory: ' + err);
          } 
          let documents=[];
          //listing all files using forEach
          files.forEach(function (file) {
              const doc=new documentModel();
              doc.name=file;
              doc.path=directoryPath+'/'+file;
              documents.push(doc);
          });

          res.json(documents);
      });
  }

 
})
/**DOWNLOAD  DOCUMENT FILE */
router.get('/gallery/:documentName', function(req, res){
  const documentName=req.params.documentName;
  let file = uploadedFilePath+'/gallery/'+documentName;
  res.download(file);  
});
module.exports = router;