var express = require('express');
var router = express.Router();
const siteVariableModel=require('../models/siteVariable')
const errorMessages=require('../error-codes');

//change
router.put('/', function(req, res, next) {
  const year=req.body.numberOfYear
  const customer=req.body.numberOfCustomer
  const city=req.body.numberOfCity
  const save=siteVariableModel.findOneAndUpdate({}, {numberOfCustomer:customer,numberOfCity:city,numberOfYear:year}, { sort: { 'created_at' : -1 } })
  save.then((variable)=>{
         res.json('Changed!');
    
  }).catch((err=>{
    console.log(err)
    res.status(500).send(errorMessages.ERROR());
  }))
});
//post
router.post('/', function(req, res, next) {
  const newVariable=new siteVariableModel(req.body)
  const saveVariables=newVariable.save()
  saveVariables.then((variable)=>{
  
         res.json('added!');
   
  }).catch((err=>{
    res.status(500).send(errorMessages.ERROR());
  }))
});

module.exports = router;
