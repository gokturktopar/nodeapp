const express=require('express');
const router = express.Router();
const bcrypt=require('bcryptjs');
const jwt=require('jsonwebtoken');
const UserModel=require('../models/user');
router.post('/',(req,res,next)=>{


const {username,password}=req.body;

if(!username||!password){
    res.json({status:-1,message:'Username and Password Fields Must Be Filled.'});
}
else{
const promise=UserModel.findOne({username:username});
    promise.then((user)=>{
      
    if(!user){//hatalı username kontrolü
        res.json({status:-1,message:'Hatalı Kullanıcı Adı! Lütfen Bilgilerinizi Kontrol Ediniz.'});
    } 
    else{
        bcrypt.compare(password,user.password).then((result) => {
            if(result){
                /**Token oluştruma */
                const payload={
                    username:username,
                    id:user._id,
                    password:user.password
                }
                //token -> payload+secretkey
                const token=jwt.sign(payload,req.app.get('apiSecretKey'),{
                    expiresIn:'1y' // 1 yıl geçerli
                });
               res.json({status:1,token:token,id:user.id,username:user.username});
            }
                else{
                res.json({status:-1,message:'Hatalı Parola! Lütfen Parolanızı Kontrol Ediniz'});//hatalı şifre
                }
        }).catch((err)=>{ // decryption error
            res.json({status:-1,message:'Decryption Error'});
        });
    }   
       

            }).catch((err)=>{//Db error
               res.json({status:-1,message:'Error! Server is not responding...'});

            });
    }
});





module.exports=router;