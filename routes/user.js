var express = require('express');
var router = express.Router();
const bcrypt=require('bcryptjs');
const userModel=require('../models/user')
const errorMessages=require('../error-codes');
/* GET ALL USER */
router.get('/', function(req, res, next) {
  const getUsers=userModel.find({})
  getUsers.then((data)=>{
      res.json(data)
  }).catch((error)=>{
    console.log(error);
    res.status(500).send(errorMessages.ERROR());//hash decrypt error
  }); 
});
/* POST USER */
router.post('/', function(req, res, next) {
  const newUser=new userModel(req.body);
   bcrypt.hash(newUser.password,10).then((passwordHash)=>{
    newUser.password=passwordHash;
     const promise=newUser.save();
     promise.then((data)=>{
       res.json('User Created!');
     }).catch((error)=>{
      console.log(error);
      res.status(500).send(errorMessages.ERROR());
     });
  }).catch((error)=>{
    console.log(error);
    res.status(500).send(errorMessages.ERROR());//hash decrypt error
  }); 
});
/*CHANGE PASSWORD */
router.put('/:userId/changepassword',(req,res,next)=>{

    const newPassword= req.body.newPassword;
    const findUser=userModel.findById(req.params.userId);
    findUser.then((user)=>{
      bcrypt.hash(newPassword,10).then((passwordHash)=>{
          user.password=passwordHash;
          const saveUser=user.save();
          saveUser.then(()=>{
          res.json('Password Changed!');
          }).catch((error)=>{//hashing error
            console.log(error);
            res.status(500).send(errorMessages.ERROR());
          });
      }).catch((error)=>{//decryption error
        console.log(error);
        res.status(500).send(errorMessages.ERROR());
      })
    }).catch((error)=>{//db error
      console.log(error);
      res.status(500).send(errorMessages.ERROR());

    })
    
    
}); 
module.exports = router;
