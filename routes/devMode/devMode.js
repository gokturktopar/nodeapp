const express = require('express');
const bcrypt=require('bcryptjs');
const errorMessages=require('../../error-codes');

const router = express.Router();
//models
const userModel=require('../../models/user');
/* POST USER */
router.post('/addUser', function(req, res, next) {
  console.log(req.body)
  const newUser=new userModel(req.body);
   bcrypt.hash(newUser.password,10).then((passwordHash)=>{
    newUser.password=passwordHash;
     const promise=newUser.save();
     promise.then((data)=>{
       res.json('User Created!');
     }).catch((error)=>{
      console.log(error);
      res.status(500).send(errorMessages.ERROR());
     });
  }).catch((error)=>{
    console.log(error);
    res.status(500).send(errorMessages.ERROR());//hash decrypt error
  }); 
});
         



module.exports = router;