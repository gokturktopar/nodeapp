const router=require('express').Router();
//model
const documentModel=require('../models/document');
const fs=require('fs');
const uploadedFilePath=require('../helper/uploadfilepath');
const errorMessages=require('../error-codes');
var webp=require('webp-converter');
/*
    PROJECT DOCUMENTS END POINTS
*/

/*POST DOCUMENT */
router.post('/',(req,res,next)=>{
  try {
    if (!req.files) {
        res.status(500).send('No File In Request');
      } 
      else {
          const dir=uploadedFilePath+'/gallery/';
          /*Klasoru Oluşturma */
          if (!fs.existsSync(dir)){
            fs.mkdirSync(dir,{recursive:true});
            
        }  
        const files=req.files.files;
        if(Array.isArray(files)){
            let noError=true;
            for (const item of files) {
                //aynı isimli file olup olmadığına bakma
                let photoName=item.name
                let existencyCounter=1
                while(fs.existsSync(dir+photoName)){
                        const photoNameArr=photoName.split('.')
                        photoName=photoNameArr[0]+existencyCounter+'.'+photoNameArr[1]
                        existencyCounter++
                }
                item.mv(dir+photoName, function(err) {
                    if (err){
                        noError=false
                        console.log(err);
                    }
                    else{
                        webp.cwebp(dir+photoName,dir+photoName+".webp","-q 80",function(status,error)
                        {
                            fs.unlink(dir+photoName, (err) => {
                                if (err) {
                                  console.error(err);
                                    noError=false

                                }
                              })
                        });
                     }
                     }
                 )}
    
                 if(noError)
                 res.json('Documents Added');                        
                 else
                 res.status(500).send('Error! Doc Not Added');
                 }
                 else{
                    const item=files;
                    let photoName=item.name
                    let existencyCounter=1
                    while(fs.existsSync(dir+photoName)){
                        const photoNameArr=photoName.split('.')
                        photoName=photoNameArr[0]+existencyCounter+'.'+photoNameArr[1]
                            existencyCounter++
                    }

                    item.mv(dir+photoName, function(err) {
                    if (err){
                        console.log('ERROR');
                     res.status(500).send('Documents Could not Added');
                    }
                     else{
                        webp.cwebp(dir+photoName,dir+photoName+".webp","-q 80",function(status,error)
                        {
                            fs.unlink(dir+photoName, (err) => {
                                if (err) {
                                  console.error(err);
                                  res.status(500).send('No Document');
                                }
                              })
                        });
                    res.json('Document Added');
                     }
             })
         }
      
     
}
  } catch (error) {
      console.log(error);
  }
    
})

/*
    Get All Documents
*/
router.get('/',(req,res,next)=>{
    const directoryPath=uploadedFilePath+'/gallery/';
    if (!fs.existsSync(directoryPath)){
        res.json(-1);
        //res.status(500).send('No Document For Customer');
    }
    else{
        fs.readdir(directoryPath, function (err, files) {  
            if (err) {
            res.json(-1);
                
                return console.log('Unable to scan directory: ' + err);
            } 
            let documents=[];
            //listing all files using forEach
            files.forEach(function (file) {
                const doc=new documentModel();
                doc.name=file;
                doc.path=directoryPath+'/'+file;
                documents.push(doc);
            });

            res.json(documents);
        });
    }
  
   
})
/*
    Delete Document
*/
router.delete('/:documentName',(req,res,next)=>{
    const directoryPath=uploadedFilePath+'/gallery';
    try {
        if (!fs.existsSync(directoryPath)){
            res.status(500).send('No Document For Customer');
        }
        else{
            fs.unlink(directoryPath+'/'+req.params.documentName, (err) => {
                if (err) {
                  console.error(err);
                  res.status(500).send('No Document');
                }
                res.json('Document deleted');
              })
        }
    } 
    catch (error) {
        console.log(error);
        res.status(500).send('Document Could Not Deleted');
        
    }
   
    })
/**DOWNLOAD  DOCUMENT FILE */
router.get('/:documentName', function(req, res){
    const documentName=req.params.documentName;
    let file = uploadedFilePath+'/gallery/'+documentName;
    res.download(file);  
  });

module.exports=router;