var express = require('express');
var router = express.Router();

const customerMessageModel=require('../models/customerMessage')
const errorMessages=require('../error-codes');

/* GET ALL MESSAGES */
router.get('/', function(req, res, next) {
  customerMessageModel.find({}).then((data)=>{res.json(data)}).catch(err=>    res.status(500).send(errorMessages.ERROR())
  )
});
/* DELETE  MESSAGE */
router.delete('/:messageId', function(req, res, next) {
  const messageId=req.params.messageId
  customerMessageModel.findByIdAndDelete(messageId).then((data)=>{res.json(data)})
  .catch((err)=>{
    res.status(500).send(errorMessages.ERROR());
    console.log(err)
  })
});

module.exports = router;