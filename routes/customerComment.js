var express = require('express');
var router = express.Router();
const customerCommentModel=require('../models/customerComment')
const errorMessages=require('../error-codes');
const fs=require('fs');
const uploadedFilePath=require('../helper/uploadfilepath');
const documentModel=require('../models/document');
const Path = require('path');
var webp=require('webp-converter');


/* POSTS NEW COMMENT */
router.post('/', function(req, res, next) {
  const newMessage=new customerCommentModel(req.body);
  const promise=newMessage.save();
  promise.then((data)=>{
      res.json(data._id);
  }).catch((err)=>{
    res.status(500).send(errorMessages.ERROR());
  });
});
/* UPDATE  COMMENT */
router.put('/:commentId', function(req, res, next) {
  const commentId=req.params.commentId
  const newMessage=new customerCommentModel(req.body);
  const update=customerCommentModel.findByIdAndUpdate(commentId,{nameSurname:newMessage.nameSurname,comment:newMessage.comment})
  update.then(data=>{
    res.json(data._id)
    const directoryPath=uploadedFilePath+'/comments/'+commentId+'/';
    deleteFolderRecursive(directoryPath)
   
  }).catch(err=>{
    res.status(500).send(errorMessages.ERROR());
  })
});

/* GET  ONE COMMENT */
router.get('/:commentId', function(req, res, next) {
  const commentId=req.params.commentId
  customerCommentModel.findById(commentId).then((data)=>{res.json(data)})
  .catch((err)=>{
    res.status(500).send(errorMessages.ERROR());
    console.log(err)
  })
});
/* DELETE  COMMENT */
router.delete('/:commentId', function(req, res, next) {
  const commentId=req.params.commentId
  customerCommentModel.findByIdAndDelete(commentId).then((data)=>{
    res.json(data)
    const directoryPath=uploadedFilePath+'/comments/'+commentId+'/';
    deleteFolderRecursive(directoryPath)
  })
  .catch((err)=>{
    res.status(500).send(errorMessages.ERROR());
  })
});


/*POST DOCUMENT */

router.post('/photo/:commentId',(req,res,next)=>{
  const commentId=req.params.commentId;
try {
  if (!req.files) {
      res.status(500).send('No File In Request');
    } 
    else {
        const dir=uploadedFilePath+'/comments/'+commentId+'/';
        /*Klasoru Oluşturma */
        if (!fs.existsSync(dir)){
          fs.mkdirSync(dir,{recursive:true});
          
      }  
      const files=req.files.files;
    
                const item=files;
                let photoName=item.name
                let existencyCounter=1
                while(fs.existsSync(dir+photoName)){
                    const photoNameArr=photoName.split('.')
                    photoName=photoNameArr[0]+existencyCounter+'.'+photoNameArr[1]
                        existencyCounter++
                }

                item.mv(dir+photoName, function(err) {
                if (err){
                    console.log('ERROR');
                 res.status(500).send('Documents Could not Added');
                }
                 else{
                    webp.cwebp(dir+photoName,dir+photoName+".webp","-q 80",function(status,error)
                    {
                        fs.unlink(dir+photoName, (err) => {
                            if (err) {
                              console.error(err);
                              res.status(500).send('No Document');
                            }
                          })
                    });
                res.json('Document Added');
                 }
         })
     
    
   
}
} catch (error) {
    console.log(error);
}
  
})


const deleteFolderRecursive = function(directoryPath) {
 try {
  if (fs.existsSync(directoryPath)) {
    fs.readdirSync(directoryPath).forEach((file, index) => {
      const curPath = Path.join(directoryPath, file);
      if (fs.lstatSync(curPath).isDirectory()) { // recurse
        deleteFolderRecursive(curPath);
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(directoryPath);
  }
 } catch (error) {
   console.log(error)
 }
};
module.exports = router;
